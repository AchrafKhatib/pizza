import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PizzaService } from "../services/pizza.service";
import { Pizza } from "../models/pizza";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-addpizza",
  templateUrl: "./addpizza.component.html",
  styleUrls: ["./addpizza.component.css"]
})
export class AddpizzaComponent implements OnInit {
  cat: any;

  categorie: string;
  size: number;
  taken: boolean;

  pizza: Pizza = new Pizza();

  constructor(
    private route: ActivatedRoute,
    private pizzaService: PizzaService,
    private router: Router
  ) {
    this.route.paramMap.subscribe(
      parametre => (this.cat = parametre.get("cat"))
    );
  }

  ngOnInit() {}

  onSubmit(form: NgForm) {
    const categorie = form.value["categorie"];
    const size = form.value["size"];
    const taken = form.value["taken"];
    this.pizzaService.add(categorie, size, taken);
    this.router.navigate(["/items/" + categorie]);
  }
}
