import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PizzaItemsComponent } from "./pizza-items/pizza-items.component";
import { AddpizzaComponent } from "./addpizza/addpizza.component";
import { EditpizzaComponent } from "./editpizza/editpizza.component";

const routes: Routes = [
  { path: "items/:cat", component: PizzaItemsComponent },
  { path: "addpizza/:cat", component: AddpizzaComponent },
  { path: "editpizza/:id", component: EditpizzaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
