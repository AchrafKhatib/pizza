import { Component } from "@angular/core";
import * as firebase from "firebase";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor() {
    // Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyDoOdn4t02q3S_IuniZ4S8U6cW4vNpVIDA",
      authDomain: "pizza-b3fe0.firebaseapp.com",
      databaseURL: "https://pizza-b3fe0.firebaseio.com",
      projectId: "pizza-b3fe0",
      storageBucket: "",
      messagingSenderId: "626768028735",
      appId: "1:626768028735:web:72dc609a715d48a1"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
