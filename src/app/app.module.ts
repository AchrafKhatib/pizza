import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FileUploadModule } from "ng2-file-upload";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { SliderComponent } from "./slider/slider.component";
import { OurpizzaComponent } from "./ourpizza/ourpizza.component";
import { PizzaItemsComponent } from "./pizza-items/pizza-items.component";
import { AddpizzaComponent } from "./addpizza/addpizza.component";
import { EditpizzaComponent } from "./editpizza/editpizza.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SliderComponent,
    OurpizzaComponent,
    PizzaItemsComponent,
    AddpizzaComponent,
    EditpizzaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxDropzoneModule,
    FileUploadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
