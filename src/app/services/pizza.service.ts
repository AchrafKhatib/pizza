import { Injectable } from "@angular/core";
import { Pizza } from "../models/pizza";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class PizzaService {
  constructor(private router: Router) {}

  pz: Pizza[] = [
    {
      id: 1,
      image: "../../assets/images/10.jpg",
      title_cat: "banniere-royal",
      size: 10,
      taken: true
    },
    {
      id: 2,
      image: "../../assets/images/20.jpg",
      title_cat: "banniere-royal",
      size: 20,
      taken: false
    },

    {
      id: 3,
      image: "../../assets/images/30.jpg",
      title_cat: "pizza-margarita",
      size: 10,
      taken: true
    },
    {
      id: 4,
      image: "../../assets/images/40.jpg",
      title_cat: "pizza-margarita",
      size: 20,
      taken: false
    },
    {
      id: 5,
      image: "../../assets/images/50.jpg",
      title_cat: "pizza-margarita",
      size: 30,
      taken: true
    },

    {
      id: 6,
      image: "../../assets/images/60.jpg",
      title_cat: "cropped-pizza",
      size: 10,
      taken: true
    },
    {
      id: 7,
      image: "../../assets/images/70.jpg",
      title_cat: "cropped-pizza",
      size: 20,
      taken: false
    },
    {
      id: 8,
      image: "../../assets/images/80.jpg",
      title_cat: "cropped-pizza",
      size: 56,
      taken: true
    },
    {
      id: 9,
      image: "../../assets/images/90.jpg",
      title_cat: "cropped-pizza",
      size: 40,
      taken: false
    }
  ];

  filterCategories(cat) {
    return this.pz.filter(function(item) {
      return item.title_cat == cat;
    });
  }
  add(categorie: string, size: number, taken: boolean) {
    const pizzaObject = {
      image: "",
      id: 0,
      title_cat: "",
      size: 0,
      taken: true
    };
    (pizzaObject.title_cat = categorie),
      (pizzaObject.size = size),
      (pizzaObject.taken = taken);
    pizzaObject.image = "";
    pizzaObject.id = this.pz[this.pz.length - 1].id + 1;
    this.pz.push(pizzaObject);
  }
  delete(pizza: Pizza) {
    const pizzaToRemove = this.pz.findIndex(object => {
      return object == pizza;
    });
    this.pz.splice(pizzaToRemove, 1);
  }

  getPizzaId(id: number) {
    const pizza = this.pz.find(objet => {
      return objet.id === id;
    });
    return pizza;
  }
}
