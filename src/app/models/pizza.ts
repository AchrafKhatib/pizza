export class Pizza {
  image: any;
  id: number;
  title_cat: string;
  size: number;
  taken: boolean = false;
}
