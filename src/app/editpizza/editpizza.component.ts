import { Component, OnInit } from "@angular/core";
import { Pizza } from "../models/pizza";
import { ActivatedRoute, Router } from "@angular/router";
import { PizzaService } from "../services/pizza.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-editpizza",
  templateUrl: "./editpizza.component.html",
  styleUrls: ["./editpizza.component.css"]
})
export class EditpizzaComponent implements OnInit {
  id: any;

  categorie: string;
  size: number;
  taken: boolean;

  pizza: Pizza = new Pizza();

  constructor(
    private route: ActivatedRoute,
    private pizzaService: PizzaService,
    private router: Router
  ) {
    this.route.paramMap.subscribe(parametre => (this.id = parametre.get("id")));
  }

  ngOnInit() {
    this.categorie = this.pizzaService.getPizzaId(+this.id).title_cat;
    this.size = this.pizzaService.getPizzaId(+this.id).size;
    this.taken = this.pizzaService.getPizzaId(+this.id).taken;
  }

  onSubmit(form: NgForm) {
    const categorie = form.value["categorie"];
    const size = form.value["size"];
    const taken = form.value["taken"];
    const pizza: Pizza = {
      image: "",
      id: this.id,
      title_cat: this.categorie,
      size: this.size,
      taken: this.taken
    };
    this.pizzaService.delete(pizza);
    this.pizzaService.add(categorie, size, taken);
    this.router.navigate(["/items/" + categorie]);
  }
}
